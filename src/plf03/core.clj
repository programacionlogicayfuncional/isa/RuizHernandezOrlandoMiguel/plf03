(ns plf03.core)

;###################COMP##########################
(defn función-comp-1 
  [] 
  ( let [ f (fn [x] (* 3 x))
          g (fn [y] (* 2 y))
          z (comp f g)
         ]
   (z 2)
   )) 

(defn función-comp-2
  []
  (let [f (fn [x] (+ 2 x))
        g (fn [y] (* 3 y))
        z (comp f g)]
    (z 2)))

(defn función-comp-3
  []
  (let [f (fn [x] (* 2 x))
        g (fn [y] (+ 3 y))
        z (comp f g)]
    (z 2)))

(defn función-comp-4
  []
  (let [f (fn [x] (+ 2 x))
        g (fn [y] (+ 3 y))
        z (comp f g)]
    (z 2)))

(defn función-comp-5
  []
  (let [f (fn [x] (- x 2))
        g (fn [y] (+ 3 y))
        z (comp f g)]
    (z 2)))

(defn función-comp-6
  []
  (let [f (fn [x] (+ x 2))
        g (fn [y] (- 3 y))
        z (comp f g)]
    (z 2)))

(defn función-comp-7
  []
  (let [f (fn [x] (- x 2))
        g (fn [y] (- y 3))
        z (comp f g)]
    (z 10)))

(defn función-comp-8
  []
  (let [f (fn [x] (- x 2))
        g (fn [y] (- y 3))
        h (fn [z] (- z 3))
        z (comp f g h)]
    (z 10)))

(defn función-comp-9
  []
  (let [f (fn [x] (+ x 2))
        g (fn [y] (- y 3))
        h (fn [z] (- z 3))
        z (comp f g h)]
    (z 10)))

(defn función-comp-10
  []
  (let [f (fn [x] (+ x 2))
        g (fn [y] (+ y 3))
        h (fn [z] (- z 3))
        z (comp f g h)]
    (z 10)))

(defn función-comp-11
  []
  (let [f (fn [x] (+ x 2))
        g (fn [y] (+ y 3))
        h (fn [z] (+ z 3))
        z (comp f g h)]
    (z 10)))

(defn función-comp-12
  []
  (let [f (fn [x] (* x 2))
        g (fn [y] (+ y 3))
        h (fn [z] (+ z 3))
        z (comp f g h)]
    (z 10)))

(defn función-comp-13
  []
  (let [f (fn [x] (* x 2))
        g (fn [y] (* y 3))
        h (fn [z] (+ z 3))
        z (comp f g h)]
    (z 10)))

(defn función-comp-14
  []
  (let [f (fn [x] (* x 2))
        g (fn [y] (* y 3))
        h (fn [z] (* z 3))
        z (comp f g h)]
    (z 10)))

(defn función-comp-15
  []
  (let [f (fn [x] (/ x 2))
        g (fn [y] (* y 3))
        h (fn [z] (* z 3))
        z (comp f g h)]
    (z 10)))

(defn función-comp-16
  []
  (let [f (fn [x] (/ x 2))
        g (fn [y] (* y 3))
        h (fn [z] (* z 3))
        z (comp f g h)]
    (z 10)))

(defn función-comp-17
  []
  (let [f (fn [x] (+ x 2))
        g (fn [y] (/ y 2))
        h (fn [z] (* z 3))
        z (comp f g h)]
    (z 10)))

(defn función-comp-18
  []
  (let [f (fn [x] (+ x 2))
        g (fn [y] (+ y 2))
        h (fn [z] (* z 3))
        z (comp f g h)]
    (z 10)))

(defn función-comp-19
  []
  (let [f (fn [x] (- x 2))
        g (fn [y] (+ y 2))
        h (fn [z] (- z 3))
        z (comp f g h)]
    (z 10)))

(defn función-comp-20
  []
  (let [f (fn [x] (+ x 2))
        g (fn [y] (/ y 2))
        h (fn [z] (+ z 30))
        z (comp f g h)]
    (z 10)))

(función-comp-1)
(función-comp-2)
(función-comp-3)
(función-comp-4)
(función-comp-5)
(función-comp-6)
(función-comp-7)
(función-comp-8)
(función-comp-9)
(función-comp-10)
(función-comp-11)
(función-comp-12)
(función-comp-13)
(función-comp-14)
(función-comp-15)
(función-comp-16)
(función-comp-17)
(función-comp-18)
(función-comp-19)
(función-comp-20)

;#############COMPLEMENT#####################################
(defn función-complement-1
  []
  (let [f (fn [x] (even? (* 2 x)))
        z (complement f)]
    (z 2)))
 
(defn función-complement-2
  []
  (let [f (fn [x] (even? x))
        z (complement f)]
    (z 10)))
 
(defn función-complement-3
  []
  (let [f (fn [x] (pos? x))
        z (complement f)]
    (z -2)))
 
(defn función-complement-4
  []
  (let [f (fn [x] (neg? x))
        z (complement f)]
    (z -6)))

(defn función-complement-5
  []
  (let [f (fn [x] (string? x))
        z (complement f)]
    (z "HOLA")))

(defn función-complement-6
  []
  (let [f (fn [x] (char? x))
        z (complement f)]
    (z \a)))

(defn función-complement-7
  []
  (let [f (fn [x] (int? x))
        z (complement f)]
    (z \a)))

(defn función-complement-8
  []
  (let [f (fn [x] (map? x))
        z (complement f)]
    (z [1 2 3])))


(defn función-complement-9
  []
  (let [f (fn [x] (indexed? x))
        z (complement f)]
    (z [1 2 3])))

(defn función-complement-10
  []
  (let [f (fn [x] (double? x))
        z (complement f)]
    (z 2.13)))

(defn función-complement-11
  []
  (let [f (fn [x] (keyword? x))
        z (complement f)]
    (z #{:a 1 :b 2})))

(defn función-complement-12
  []
  (let [f (fn [x] (number? x))
        z (complement f)]
    (z 4)))

(defn función-complement-13
  []
  (let [f (fn [x] (list? x))
        z (complement f)]
    (z '(1 2 3))))

(defn función-complement-14
  []
  (let [f (fn [x] (vector? x))
        z (complement f)]
    (z [1 2 3 4 5])))

(defn función-complement-15
  []
  (let [f (fn [x] (boolean? x))
        z (complement f)]
    (z true)))

(defn función-complement-16
  []
  (let [f (fn [x] (odd? x))
        z (complement f)]
    (z 7)))

(defn función-complement-17
  []
  (let [f (fn [x] (odd? x))
        g (fn [y] (not (f y)))
        z (complement g)]
    (z 8)))

(defn función-complement-18
  []
  (let [f (fn [x] (pos-int? x))
        g (fn [y] (not (f y)))
        z (complement g)]
    (z -18)))

(defn función-complement-19
  []
  (let [f (fn [x] (neg-int? x))
        g (fn [y] (not (f y)))
        z (complement g)]
    (z 18)))


(defn función-complement-20
  []
  (let [f (fn [x] (string? x))
        g (fn [y] (not (f y)))
        z (complement g)]
    (z :a)))


(función-complement-1)
(función-complement-2)
(función-complement-3)
(función-complement-4)
(función-complement-5)
(función-complement-6)
(función-complement-7)
(función-complement-8)
(función-complement-9)
(función-complement-10)
(función-complement-11)
(función-complement-12)
(función-complement-13)
(función-complement-14)
(función-complement-15)
(función-complement-16)
(función-complement-17)
(función-complement-18)
(función-complement-19)
(función-complement-20)

;#############CONSTANTLY#####################################
(defn función-constantly-1
  []
  (let [f "Texto Constante 1"
        z (constantly f)]
  (z [1 2 3])
    ))

 (defn función-constantly-2
  []
  (let [f "Texto Constante 2"
        z (constantly f)]
  (z 10)
    ))

(defn función-constantly-3
   []
   (let [f "Texto Constante 3"
         g (fn [] (* 2 2))
         z (constantly f)]
     (z b)))

 (defn función-constantly-4
   []
   (let [f "Texto Constante 4"
         g 10
         h(fn [] (+ g 3))
         z (constantly f)]
     (z h)))
 
 (defn función-constantly-5
   []
   (let [f "Texto Constante 5"
         g 5
         h (fn [] (* 2 g))
         z (constantly f)]
     (z h)))

 (defn función-constantly-6
   []
   (let [f (* 10 2)
         g (fn [] ( * 2 2))
         h (fn [] (+ g g))
         z (constantly f)
         ]
     (z h)))
 
 (defn función-constantly-7
   []
   (let [f (* 10 1)
         g (fn [] (* 10 10))
         h (fn [] (+ g g))
         i (fn [] (/ h 2))
         z (constantly f)]
     (z i)))
 
 (defn función-constantly-8
   []
   (let [f :a
         g (fn [] (* 10 10))
         h (fn [] (+ g g))
         i (fn [] (/ h 2))
         z (constantly f)]
     (z i)))
 
 (defn función-constantly-9
   []
   (let [f [:a 1 :b 2]
         g (fn [] (* 10 1))
         z (constantly f)]
     (z g)))
 
 (defn función-constantly-10
   []
   (let [f [:a 1 :b 2]
         z (constantly f)]
     (z "argumento")))

  (defn función-constantly-11
    []
    (let [f [:a 1 :b 2]
          z (constantly f)]
      (z [1 2 3])))
 
 (defn función-constantly-12
   []
   (let [f "Texto constante 12"
         g (fn [] (* 2 2))
         h (fn [] (* g g))
         z (constantly f)]
     (z h)))
 
 (defn función-constantly-13
   []
   (let [f "Texto constante 13"
         g (fn [] (* 2 2))
         h (fn [] (* g g))
         z (constantly f)]
     (z \a)))
 
 (defn función-constantly-14
   []
   (let [f "Texto constante 14"
         g (fn [] (* 2 2))
         h (fn [] (* g g))
         z (constantly f)]
     (z :a))) 
 
  (defn función-constantly-15
    []
    (let [f "Texto constante 15"
          g (fn [] (* 2 2))
          h (fn [] (* g g))
          z (constantly f)]
      (z :a)))
 
  (defn función-constantly-16
    []
    (let [f "Texto constante 16"
          g (fn [] (* 2 2))
          h (fn [] (* g g))
          z (constantly f)]
      (z #{\a \b})))
 
  (defn función-constantly-17
    []
    (let [
          f (fn [] (* 2 2))
          g (fn [] (* f f))
          z (constantly "Texto Constante 17")]
      (z #{\a \b})))
  
  (defn función-constantly-18
    []
    (let [f (* 2 2 2)
          z (constantly f)]
      (z #{\a \b})))
  
  (defn función-constantly-19
    []
    (let [f (* 2 2 3)
          z (constantly f)]
      (z "Argumento ejemplo")))
  
(defn función-constantly-20
  []
  (let [f "Constante"
        z (constantly f)]
    (z "argumento")))
 
 (función-constantly-1)
 (función-constantly-2)
 (función-constantly-3)
 (función-constantly-4)
 (función-constantly-5)
 (función-constantly-6)
 (función-constantly-7)
 (función-constantly-8)
 (función-constantly-9)
 (función-constantly-10)
 (función-constantly-11)
 (función-constantly-12)
 (función-constantly-13)
 (función-constantly-14)
 (función-constantly-15)
 (función-constantly-16)
 (función-constantly-17)
 (función-constantly-18)
 (función-constantly-19)
 (función-constantly-20)
 
;#############EVERY-PRED#####################################
(defn funcion-every-pred-1
  []
  (let [f (fn [x] (odd? x))
        g (fn [y] (number? (f y)))
        z (every-pred f)]
    (z 3)))

(defn funcion-every-pred-2
  []
  (let [f (fn [y] ((comp (partial > 5) count) y))
        g (fn [x] (string? x))
        z (every-pred g f)]
    (z "abc")))
 
 (defn funcion-every-pred-3
   []
   (let [f (fn [x] (number? x))
         g (fn [y] (pos? y))
         z (every-pred f g)]
     (z 10)))

  (defn funcion-every-pred-4
    []
    (let [f (fn [x] (char? x))
          g (fn [y] (char? y))
          z (every-pred f g)]
      (z \a))) 
 
 (defn funcion-every-pred-5
   []
   (let [f (fn [x] (odd? x))
         g (fn [y] (pos? y))
         z (every-pred f g)]
     (z 3)))

 (defn funcion-every-pred-6
   []
   (let [f (fn [x] (odd? x))
         g (fn [y] (neg? y))
         z (every-pred f g)]
     (z -3)))
 
 (defn funcion-every-pred-7
   []
   (let [f (fn [x] (even? x))
         g (fn [y] (pos? y))
         z (every-pred f g)]
     (z 2))) 
 (defn funcion-every-pred-8
   []
   (let [f (fn [x] (even? x))
         g (fn [y] (neg? y))
         z (every-pred f g)]
     (z -2))) 
 
 (defn funcion-every-pred-9
   []
   (let [f (fn [x] (pos? x))
         g (fn [y] (double? y))
         z (every-pred f g)]
     (z 20.10)))
 
 (defn funcion-every-pred-10
   []
   (let [f (fn [x] (neg? x))
         g (fn [y] (double? y))
         z (every-pred f g)]
     (z 20.10)))
 
  (defn funcion-every-pred-11
    []
    (let [f (fn [x] (true? x))
          g (fn [y] (boolean? y))
          z (every-pred f g)]
      (z true)))
 
 (defn funcion-every-pred-12
   []
   (let [f (fn [x] (false? x))
         g (fn [y] (boolean? y))
         z (every-pred f g)]
     (z false)))
 
  (defn funcion-every-pred-13
    []
    (let [f (fn [x] (false? x))
          g (fn [y] (boolean? y))
          z (every-pred f g)]
      (z false)))
 
   (defn funcion-every-pred-14
     []
     (let [f (fn [x] (number? x))
           g (fn [y] (char? y))
           z (every-pred f g)]
       (z \1))) 
  
 (defn funcion-every-pred-15
   []
   (let [f (fn [x] (keyword? x))
         g (fn [y] (symbol? y))
         z (every-pred f g)]
     (z (:a "abc"))))
 
 (defn funcion-every-pred-16
   []
   (let [f (fn [x] (indexed? x))
         g (fn [y] (map? y))
         z (every-pred f g)]
     (z '(:a 1 :b 2)))) 
   
   (defn funcion-every-pred-17
     []
     (let [f (fn [x] (vector? x))
           g (fn [y] (associative? y))
           z (every-pred f g)]
       (z [1 2 3])))
 
 (defn funcion-every-pred-18
 []
     (let [f (fn [x] (map? x))
           g (fn [y] (associative? y))
           z (every-pred f g)]
       (z {:a 1 :b 2})))
 
 (defn funcion-every-pred-19
[]
     (let [f (fn [x] (set? x))
           g (fn [y] (associative? y))
           z (every-pred f g)]
       (z #{:a :b})))
 
   (defn funcion-every-pred-20
     []
     (let [f (fn [x] (list? x))
           g (fn [y] (associative? y))
           z (every-pred f g)]
       (z '(1 2 3))))
   
(funcion-every-pred-1)
(funcion-every-pred-2)
(funcion-every-pred-3)
(funcion-every-pred-4)
(funcion-every-pred-5)
(funcion-every-pred-6)
(funcion-every-pred-7)
(funcion-every-pred-8)
(funcion-every-pred-9)
(funcion-every-pred-10)
(funcion-every-pred-11)
(funcion-every-pred-12)
(funcion-every-pred-13)
(funcion-every-pred-14)
(funcion-every-pred-15)
(funcion-every-pred-16)
(funcion-every-pred-17)
(funcion-every-pred-18)
(funcion-every-pred-19)
(funcion-every-pred-20)

;#############fnill#####################################

(defn función-fnil-1
  []
  (let [f (fn [x y] (if (< y x) (/ x y) (* x y)))
        z (fnil f 50)]
    (z nil 10)))

(defn función-fnil-2
  []
  (let [f (fn [x y] (+ x y))
        z (fnil f nil nil)]
    (z 30 30)))

(defn función-fnil-3
  []
  (let [f (fn [x] (take 3 x))
        z (fnil f (take 2 (range 10 20)))]
    (z nil)))

(defn función-fnil-4
  []
  (let [f (fn [x] (empty? x))
        g (fn [x] (first x))
        y (fnil f nil)
        z (fnil g nil)]
    [(y [1 2 3 4 5]) (z [5 4 3 2 1])]))

(defn función-fnil-5
  []
  (let [f (fn [x] (str "cadena 1 " x))
        z (fnil f "cadena 2")]
    (z nil)))

(defn función-fnil-6
  []
  (let [f (fn [x] (map false? x))
        z (fnil f (map false? [false true false true]))]
    (z ["true" true true nil])))

(defn función-fnil-7
  []
  (let [f (fn [x] (take-last 5 x))
        z (fnil f (take-last 5 (range 50 100)))]
    (z '(0 2 4 6 8 10 12 14 16 18 20))))

(defn función-fnil-8
  []
  (let [f (fn [x] (nil? x))
        z (fnil f (nil? nil))]
    (z nil)))

(defn función-fnil-9
  []
  (let [f (fn [a b c] (str "Name: " a " " b " " c))
        z (fnil f "Nappa" "Flores" "Dominguez")]
    (z nil "Nappa" nil)))

(defn función-fnil-10
  []
  (let [f (fn [x] (take-last 2 x))
        z (fnil f (take-last 2 (range 10 50)))]
    (z '(0 2 4 6 8 10 12 14 16 18 20))))


(defn función-fnil-11
  []
  (let [f (fn [x] (take-last 20 x))
        y (fnil f (take-nth 100 (range 100 500)))
        z (fnil f (take-last 10 (range 50 100)))]
    [(y nil) (z nil)]))

(defn función-fnil-12
  []
  (let [f (fn [x y] (str "Prueba: " x " y " y))
        z (fnil f "Cadena1" "Cadena2")]
    (z "Cadena4" "Cadena3")))

(defn función-fnil-13
  []
  (let [f (fn [x] (reverse x))
        z (fnil f (+ 2 3))]
    (z "ocaidoz led sorellabac sol")))

(defn función-fnil-14
  []
  (let [f (fn [x] (take 3 x))
        z (fnil f (take 2 (range 5)))]
    (z [\a \e \i \o \u])))

(defn función-fnil-15
  []
  (let [f (fn [x] (map boolean? x))
        z (fnil f "nulo")]
    (z #{false true})))

(defn función-fnil-16
  []
  (let [f (fn [a] (dec a))
        g (fn [a b] (- a b))
        h (fn [a b c] (* a b c))
        x (fnil f 110)
        y (fnil g 1/2 1/8)
        z (fnil h 98 12 897)]
    [(x nil) (y 6 nil) (z 8/5 nil 3/4)]))

(defn función-fnil-17
  []
  (let [f (fn [x] (str "String 1 " x))
        z (fnil f "String 2")]
    (z "String 3")))

(defn función-fnil-18
  []
  (let [f (fn [x y] (if (> x y) (+ x y) (* x y)))
        z (fnil f 50)]
    (z nil 10)))

(defn función-fnil-19
  []
  (let [f (fn [x] (str "Color " x))
        z (fnil f "tono")]
    (z nil)))

(defn función-fnil-20
  []
  (let [f (fn [w x y] (replace [:a :b :c :d :e :f] [w x y]))
        z (fnil f 1 1 1)]
    (z nil nil nil)))


(función-fnil-1)
(función-fnil-2)
(función-fnil-3)
(función-fnil-4)
(función-fnil-5)
(función-fnil-6)
(función-fnil-7)
(función-fnil-8)
(función-fnil-9)
(función-fnil-10)
(función-fnil-11)
(función-fnil-12)
(función-fnil-13)
(función-fnil-14)
(función-fnil-15)
(función-fnil-16)
(función-fnil-17)
(función-fnil-18)
(función-fnil-19)
(función-fnil-20)
;#############juxt#####################################
(defn función-juxt-1
  []
  (let [a :a
           z ( juxt a)]
    (z {:a 1 :b 2 :c 3 :d 4})))
 
(defn función-juxt-2
  []
  (let [a (fn [x] ({:a 1 :b 2 :c 3 :d 4} x))
        b (fn [y] (a y))
        z (juxt b)]
    (z :b)))
 
(defn función-juxt-3
  []
  (let [f (fn [x] (take 3 x))
        g (fn [y] (drop 4 y))
        z (juxt f g)]
    (z [1 2 3 4 5])))

(defn función-juxt-4
  []
  (let [f (fn [x] (vector x))
        g (fn [y] (filter odd? y))
        z (juxt g f)]
    (z #{3 4 5 6})))

(defn función-juxt-5
  []
  (let [f (fn [x] (count x))
        g (fn [y] (first y))
        z (juxt g f)]
    (z [:a 10 :b 22])))
 
(defn función-juxt-6
  []
  (let [f (fn [x] (- 10 x))
        g (fn [y] (- 10 y))
        h (fn [z] (inc  z))
        z (juxt h g f)]
    (z 2)))

(defn función-juxt-7
  []
  (let [f (fn [x] (filter char? x))
        g (fn [y] (count y))
        h (fn [z] (vector z))
        z (juxt h g f)]
    (z #{1 "uno" 2 "dos" 3 "tres"})))

(defn función-juxt-8
  []
  (let [f (fn [x] (last x))
        g (fn [y] (list y))
        h (fn [z] (conj [10] z))
        z (juxt h g f)]
    (z [60 50 40 50])))

(defn función-juxt-9
  []
  (let [f (fn [x] (take 2 x))
        g (fn [y] (reverse y))
        z (juxt g f)]
    (z "ocaidoz")))

(defn función-juxt-10
  []
  (let [f (fn [x] (range x))
        g (fn [y] (- 40 y))
        h (fn [z] (int? z))
        z (juxt f h g)]
    (z 75)))


(defn función-juxt-10
  []
  (let [f (fn [xs] (range xs))
        g (fn [x] (- 40 x))
        h (fn [x] (int? x))
        z (juxt f h g)]
    (z 75)))

(defn función-juxt-11
  []
  (let [f (fn [x] (map key x))
        g (fn [y] (map val y))
        z (juxt f g)]
    (z {:a \a :b \b :c \c})))

(defn función-juxt-12
  []
  (let [f (fn [x] (count x))
        g (fn [y] (map val y))
        z (juxt f g)]
    (z {:a 1 :b 2 :c 3})))

(defn función-juxt-13
  []
  (let [f (fn [x] (sort x))
        g (fn [y] (conj [\a \b] y))
        z (juxt f g)]
    (z "5432")))

(defn función-juxt-14
  []
  (let [f (fn [x] (sort x))
        g (fn [y] (conj [\a \b \c] y))
        z (juxt f g)]
    (z "Testing")))

(defn función-juxt-15
  []
  (let [f (fn [x] (remove true? x))
        g (fn [y] (last y))
        h (fn [z] (filter boolean? z))
        z (juxt g f h)]
    (z '(true \a true "num" false 0.5 true 1M))))

(defn función-juxt-16
  []
  (let [f (fn [xs] (vector? xs))
        g (fn [x] (list? x))
        h (fn [x] (map? x))
        z (juxt g f h)]
    (z '("uno" "dos" "tres" "cuatro"))))

(defn función-juxt-17
  []
  (let [f (fn [xs] (take-last 2 xs))
        g (fn [x] (update x 2 inc))
        h (fn [x] (count x))
        z (juxt g f h)]
    (z [10 20 30 40])))

(defn función-juxt-18
  []
  (let [f (fn [x] (drop-last x))
        g (fn [y] (remove neg? y))
        z (juxt g f)]
    (z #{-1 -2 -3 -4 5})))

(defn función-juxt-19
  []
  (let [f (fn [x] (sort-by count x))
        g (fn [y] (string? y))
        z (juxt f g)]
    (z ["Auron" "Bjean" "Calvoo" "Rubiuh"])))

(defn función-juxt-20
  []
  (let [f (fn [x] (count x))
        g (fn [y] (last y))
        z (juxt g f)]
    (z [:a 1 :b 2])))


(función-juxt-1) 
(función-juxt-2)
(función-juxt-3)
(función-juxt-4)
(función-juxt-5)
(función-juxt-6)
(función-juxt-7)
(función-juxt-8)
(función-juxt-9)
(función-juxt-10)
(función-juxt-11)
(función-juxt-12)
(función-juxt-13)
(función-juxt-14)
(función-juxt-15)
(función-juxt-16)
(función-juxt-17)
(función-juxt-18)
(función-juxt-19)
(función-juxt-20)
 
 ;#############partial#####################################
(defn función-partial-1
  []
  (let [f (fn [xs] (+ 100 xs))
        z (partial f)]
    (z 5)))

(defn función-partial-2
  []
  (let [f (fn [x] (* 100 x))
        z (partial f)]
    (z 5)))

(defn función-partial-3
  []
  (let [f (fn [x] (- 100 x))
        z (partial f)]
    (z 10)))

(defn función-partial-4
  []
  (let [f (fn [x] (/ 100 x))
        z (partial f)]
    (z 5)))

(defn función-partial-5
  []
  (let [f (fn [x y z] (hash-set x y z))
        z (partial f)]
    (z 1 2 3)))

(defn función-partial-6
  []
  (let [f (fn [x y z] (vector x y z))
        z (partial f)]
    (z 4 5 6)))

(defn función-partial-7
  []
  (let [f (fn [x y z] (list x y z))
        z (partial f)]
    (z 7 8 9)))

(defn función-partial-8
  []
  (let [f (fn [w x y z] (hash-map w x y z))
        z (partial f)]
    (z 10 20 30 40)))

(defn función-partial-9
  []
  (let [f (fn [x] (filter char? x))
        z (partial f)]
    (z "hola")))

(defn función-partial-10
  []
  (let [f (fn [x] (group-by char? x))
        z (partial f)]
    (z "12345")))

(defn función-partial-11
  []
  (let [f (fn [x] (sort-by char? x))
        z (partial f)]
    (z "hola")))

(defn función-partial-12
  []
  (let [f (fn [x] (take-while char? x))
        z (partial f)]
    (z "hola")))

(defn función-partial-13
  []
  (let [f (fn [x] (sequential? x))
        z (partial f)]
    (z "hola")))

(defn función-partial-14
  []
  (let [f (fn [x] (remove char? x))
        z (partial f)]
    (z "hola")))

(defn función-partial-15
  []
  (let [f (fn [x] (filterv even? x))
        z (partial f)]
    (z [10 20 30 40])))

(defn función-partial-16
  []
  (let [f (fn [x] (filter neg? x))
        z (partial f)]
    (z [-10 20 -30 40])))

(defn función-partial-17
  []
  (let [f (fn [x] (filter pos? x))
        z (partial f)]
    (z [-10 20 -30 40])))

(defn función-partial-18
  []
  (let [f (fn [x] (take-while string? x))
        z (partial f)]
    (z ["hola" -10 20 -30 40])))

(defn función-partial-19
  []
  (let [f (fn [x] (take-while number? x))
        z (partial f)]
    (z [-10 5.5 -30 false true])))

(defn función-partial-20
  []
  (let [f (fn [x] (take-while boolean? x))
        z (partial f)]
    (z [false true -10 5.5])))

(función-partial-1)
(función-partial-2)
(función-partial-3)
(función-partial-4)
(función-partial-5)
(función-partial-6)
(función-partial-7)
(función-partial-8)
(función-partial-9)
(función-partial-10)
(función-partial-11)
(función-partial-12)
(función-partial-13)
(función-partial-14)
(función-partial-15)
(función-partial-16)
(función-partial-17)
(función-partial-18)
(función-partial-19)
(función-partial-20)
;#############some fn#####################################

(defn función-some-fn-1 
  [] 
  (let [ f (fn [x] (pos? x))
         g (fn [y] (even? y))
         h (fn [y] (int? y))
         z (some-fn f g h) 
        ]
    (z 10)))

(defn función-some-fn-2
  []
  (let [f (fn [x] (pos? x))
        g (fn [y] (even? y))
        h (fn [y] (double? y))
        z (some-fn f g h)]
    (z -7)))

(defn función-some-fn-3
  []
  (let [f (fn [x] (pos? x))
        g (fn [y] (even? y))
        h (fn [y] (decimal? y))
        z (some-fn f g h)]
    (z -7)))

(defn función-some-fn-4
  []
  (let [f (fn [x] (pos? x))
        g (fn [y] (odd? y))
        h (fn [y] (int? y))
        z (some-fn f g h)]
    (z -4)))

(defn función-some-fn-5
  []
  (let [f (fn [x] (pos? x))
        g (fn [y] (odd? y))
        h (fn [y] (double? y))
        z (some-fn f g h)]
    (z -4)))

(defn función-some-fn-6
  []
  (let [f (fn [x] (pos? x))
        g (fn [y] (odd? y))
        h (fn [y] (decimal? y))
        z (some-fn f g h)]
    (z -4)))

(defn función-some-fn-7
  []
  (let [f (fn [x] (neg? x))
        g (fn [y] (even? y))
        h (fn [y] (int? y))
        z (some-fn f g h)]
    (z 10)))

(defn función-some-fn-8
  []
  (let [f (fn [x] (neg? x))
        g (fn [y] (even? y))
        h (fn [y] (double? y))
        z (some-fn f g h)]
    (z -7)))

(defn función-some-fn-9
  []
  (let [f (fn [x] (neg? x))
        g (fn [y] (even? y))
        h (fn [y] (decimal? y))
        z (some-fn f g h)]
    (z -7)))

(defn función-some-fn-10
  []
  (let [f (fn [x] (neg? x))
        g (fn [y] (odd? y))
        h (fn [y] (int? y))
        z (some-fn f g h)]
    (z -4)))

(defn función-some-fn-11
  []
  (let [f (fn [x] (neg? x))
        g (fn [y] (odd? y))
        h (fn [y] (double? y))
        z (some-fn f g h)]
    (z -4)))

(defn función-some-fn-12
  []
  (let [f (fn [x] (neg? x))
        g (fn [y] (odd? y))
        h (fn [y] (decimal? y))
        z (some-fn f g h)]
    (z -4)))
;##########

(defn función-some-fn-13
  []
  (let [f (fn [x] (pos? x))
        g (fn [y] (decimal? y))
        z (some-fn f g)]
    (z -4)))

(defn función-some-fn-14
  []
  (let [f (fn [x] (pos? x))
        g (fn [y] (int? y))
        z (some-fn f g)]
    (z -4)))

(defn función-some-fn-15
  []
  (let [f (fn [x] (neg? x))
        g (fn [y] (double? y))
        z (some-fn f g)]
    (z 4)))

(defn función-some-fn-16
  []
  (let [f (fn [x] (neg? x))
        g (fn [y] (decimal? y))
        z (some-fn f g)]
    (z 4)))

(defn función-some-fn-17
  []
  (let [f (fn [x] (neg? x))
        g (fn [y] (int? y))
        z (some-fn f g)]
    (z 4)))

(defn función-some-fn-18
  []
  (let [f (fn [x] (pos? x))
        g (fn [y] (double? y))
        z (some-fn f g)]
    (z -4)))

(defn función-some-fn-19
  []
  (let [f (fn [x] (even? x))
        g (fn [y] (int? y))
        z (some-fn f g)]
    (z -4)))

(defn función-some-fn-20
  []
  (let [f (fn [x] (odd? x))
        g (fn [y] (int? y))
        z (some-fn f g)]
    (z -4)))

(función-some-fn-1)
(función-some-fn-2)
(función-some-fn-3)
(función-some-fn-4)
(función-some-fn-5)
(función-some-fn-6)
(función-some-fn-7)
(función-some-fn-8)
(función-some-fn-9)
(función-some-fn-10)
(función-some-fn-11)
(función-some-fn-12)
(función-some-fn-13)
(función-some-fn-14)
(función-some-fn-15)
(función-some-fn-16)
(función-some-fn-17)
(función-some-fn-18)
(función-some-fn-19)
(función-some-fn-20)



